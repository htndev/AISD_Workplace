// (c) Alexandr Vanilchuk a.k.a Alexandr Heathen

#include "stdafx.h"

int main()
{
	signed short data;
	short value;
	char sign = 0;
	printf("Enter your value: "); scanf_s("%hu", &value);
	if (value < 0)
	{
		sign = 1;
		data = -value;
	}
	else
		data = value;
	printf("0 - positive value\n1 - negative value\nYou entered: %d\nSign: %d\n", data, sign);
	return 0;
}