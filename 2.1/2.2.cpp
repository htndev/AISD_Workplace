// (c) Alexandr Vanilchuk a.k.a Alexandr Heathen

#include "stdafx.h"

int main()
{
	unsigned short data;
	signed short value;
	printf("Enter your value: "); scanf_s("%hu", &value);
	bool sign;
	sign = (value < 0);
	if (sign)
		data = -value;
	else
		data = value;
	printf("0 - positive value\n1 - negative value\nYou entered: %d\nSign: %d\n", data, sign);
	return 0;
}
