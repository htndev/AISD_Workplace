// (c) Alexandr Vanilchuk a.k.a Alexandr Heathen

#include "stdafx.h"

union data
{
	struct
	{
		unsigned char low;
		unsigned char loww;
		unsigned char high;
		unsigned char highh;
	}byte;
	float value;
};

void binaryCode(unsigned char i)
{
	unsigned char num[8];
	for (int k = 0; k < 8; k++)
	{
		num[k] = i % 2;
		i = i / 2;
	}
	for (int k = 7; k >= 0; k--)
		printf("%d", num[k]);
}

void binaryForward(unsigned char i, int numFst, int numSnd)
{
	unsigned char num[8];
	for (int k = 0; k < 8; k++)
	{
		num[k] = i % 2;
		i = i / 2;
	}
	for (int k = numFst; k <= numSnd; k++)
		printf("%d", num[k]);
}

void binaryBack(unsigned char i, int numFst, int numSnd)
{
	unsigned char num[8];
	for (int k = 0; k < 8; k++)
	{
		num[k] = i % 2;
		i = i / 2;
	}
	for (int k = numFst; k >= numSnd; k--)
		printf("%d", num[k]);
}


int main()
{
	data pointer;
	int sign = 7;
	printf("Enter your value: "); scanf_s("%f", &pointer.value);
	printf("~~~~~~~~~~~~~ Memory: %lu ~~~~~~~~~~~~~\n", sizeof(data));
	printf("Bit values: \n");
	binaryCode(pointer.byte.highh);
	printf(" ");
	binaryCode(pointer.byte.high);
	printf(" ");
	binaryCode(pointer.byte.loww);
	printf(" ");
	binaryCode(pointer.byte.low);
	printf("\n");
	printf("Low: %d ", pointer.byte.low);
	printf("Lower: %d ", pointer.byte.loww);
	printf("High: %d ", pointer.byte.high);
	printf("Higher: %d\n", pointer.byte.highh);
	printf("Sign: ");
	binaryForward(pointer.byte.highh, sign, sign);
	printf("\nCharacteristic: ");
	binaryBack(pointer.byte.highh, 6, 0);
	binaryForward(pointer.byte.high, sign, sign);
	printf("\nManstis: ");
	binaryBack(pointer.byte.high, 6, 0); printf(" ");
	binaryCode(pointer.byte.loww); printf(" ");
	binaryCode(pointer.byte.low);
	printf("\n");
	return 0;
}